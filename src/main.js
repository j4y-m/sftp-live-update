#!/usr/bin/env node
const Queue = require('./queue')
const chokidar = require('chokidar');
const path = require('path');
const fs = require('fs');
const SFTPClient = require('ssh2-sftp-client');

const main = async () => {
    let configFile = '.sftp-live-update.json'
    if (process.argv.length > 2) configFile = process.argv[2];

    if (!fs.existsSync(configFile)) {
        console.info('Usage: sftp-live-update [config]');
        console.info('default config: `.sftp-live-update.json`');
        return;
    }

    const config = JSON.parse(fs.readFileSync(configFile, { encoding: 'utf8' }));
    if (config.sftp.privateKey) {
        if (fs.existsSync(config.sftp.privateKey)) {
            config.sftp.privateKey = fs.readFileSync(config.sftp.privateKey);
        }
    }
    config.watchDir = path.resolve(path.normalize(config.watchDir));

    const sftp = new SFTPClient();
    await sftp.connect(config.sftp);
    const watcher = chokidar.watch(config.watchDir, {
        awaitWriteFinish: true,
        persistent: true,
        ignored: config.ignore.map(p => path.join(config.watchDir, p)),
    });

    const queue = new Queue();

    const handleAddDir = (path) => {
        queue.addToQueue(async () => {
            const target = path.replace(config.watchDir, config.targetDir);
            console.log('ADD DIRECTORY ' + target);
            await sftp.mkdir(target, true);
        })
    };

    const handleAddFile = (eventName) => {
        return (path) => {
            queue.addToQueue(async () => {
                const target = path.replace(config.watchDir, config.targetDir);
                const dir = target.substr(0, target.lastIndexOf('/'));
                console.log(eventName + ' ' + target);
                await sftp.mkdir(dir, true);
                await sftp.put(path, target, {
                    flags: 'w',
                    encoding: null,
                    mode: 0o666,
                    autoClose: true,
                });
            });
        }
    };

    const handleFileDelete = (path) => {
        queue.addToQueue(async () => {
            const target = path.replace(config.watchDir, config.targetDir);
            console.log('DELETE FILE ' + target);
            await sftp.delete(target);
        });
    };

    const handleDirDelete = (path) => {
        queue.addToQueue(async () => {
            const target = path.replace(config.watchDir, config.targetDir);
            console.log('DELETE DIRECTORY ' + target);
            await sftp.rmdir(target);
        });
    };

    watcher.on('addDir', handleAddDir);
    watcher.on('add', handleAddFile('ADD FILE'));
    watcher.on('change', handleAddFile('UPDATE FILE'));
    watcher.on('unlink', handleFileDelete);
    watcher.on('unlinkDir', handleDirDelete);
}

main();
