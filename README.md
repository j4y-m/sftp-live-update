# sftp-live-update

This tool allows you to listen to changes in a local folder and instantly transfer them to an SFTP server.

## Use case & Genesis

When I work hard on a project with database inside docker, backend and multiple front end, 
my laptop said something like "I don't get paid enough for this bullshit" and it starts to grumble.
Lucky as I am, I have an unused linux server at my home. With `sftp-live-update` I will show you
how I use this server to run my backend project while I'm still working on my local copy.
It allows me to save my precious memory on my computer.

## Requirements
 - Obviously an SFTP server
 - A folder to watch : in my case a yarn project

## Installation
```
yarn global add sftp-live-update
```

## Usage
Create a config file (json):
```js
{
  "sftp": {
    "host": "your sftp host",
    "port": 22,
    "username": "your sftp user",

    "privateKey": "/Users/gtaja/.ssh/id_rsa", // You can put a file or the content of your key
    "passphrase": "your passphrase",
    // or 
    "password": "your password",
    
    // You can also pass any parameters accepted by the connect function
    // of https://github.com/theophilusx/ssh2-sftp-client
  },
  "watchDir": "directory to watch in your laptop",
  "targetDir": "directory to target in your SFTP server",
  "ignore": [
    // Ignore theses files : anymatch-compatible definition
    // https://github.com/es128/anymatch
    "/node_modules",
    "/dist",
    "/.git",
    "/.idea"
  ]
}
```

Run `sftp-live-update <config>` (make sure you have add yarn binary to PATH `export PATH="$(yarn global bin):$PATH"`)  
**WARNING:** This will update every files in your SFTP server under the targetDir, make sure this dir doesn't
contains important things.  

Tricks: running `sftp-live-update` without config file will look in the file `.sftp-live-update.json`

For my use case, I log into my server in SSH and I run `yarn install && yarn start:dev` : when I
update my local copy, the server restart !

Enjoy ;)
